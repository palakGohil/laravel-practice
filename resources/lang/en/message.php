<?php

return [
    'companies' => 'Companies',
    'employees' => 'Employees',
    'company_name' => 'Company Name',
    'email' => 'Email',
    'website' => 'Website',
    'logo' => 'Logo',
    'actions' => 'Actions',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'create_company' => 'Create Company',
    'add_company' => 'Add Company',
    'edit_company' => 'Edit Company',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'company_name' => 'Company Name',
    'phone_number' => 'Phone Number',
    'edit_employee' => 'Edit Employee',
    'add_employee' => 'Add Employee',
];
