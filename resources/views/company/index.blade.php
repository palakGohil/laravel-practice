@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3>{{ trans('message.companies') }}</h3>
        <a href="{{route('create_company')}}" class="btn btn-primary">{{ trans('message.create_company') }}</a>
    </div>
    @if(session()->get('success_message'))
    <div class="alert alert-success flash-msg" role="alert">
        <button type="button" class="close alert-success" data-dismiss="alert">×</button>
        {{ session()->get('success_message') }}
    </div>
    @endif
    @if(session()->get('fail_message'))
    <div class="alert alert-success flash-msg" role="alert">
        <button type="button" class="close alert-success" data-dismiss="alert">×</button>
        {{ session()->get('fail_message') }}
    </div>
    @endif
    <div class="card-body">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
        {{ $companies->links() }}
        </ul>
    </nav>

        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{ trans('message.company_name') }}</th>
                    <th scope="col">{{ trans('message.email') }}</th>
                    <th scope="col">{{ trans('message.website') }}</th>
                    <th scope="col">{{ trans('message.logo') }}</th>
                    <th scope="col">{{ trans('message.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($companies as $key => $company)
                <tr>
                    <th>{{ $key + 1 }}</th>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->email }}</td>
                    <td>{{ $company->website }}</td>
                    <td><img src="{{asset('storage/logos/original').'/'.$company->logo}}" height="50" weidth="50"></td>
                    <td><a href="{{ route('edit_company',['id' => $company->id]) }}" class="btn btn-success"> <img src="{{ asset('images/edit_black_24dp.svg') }}" alt="edit" height="15"> {{ trans('message.edit') }}</a>&nbsp;&nbsp;<a href="{{ route('delete_company',['id' => $company->id]) }}" onClick="javascript: return confirm('Are you sure you want to delete?');" class="btn btn-danger"> <img src="{{ asset('images/delete_black_24dp.svg') }}" alt="edit" height="15"> {{ trans('message.delete') }}</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection