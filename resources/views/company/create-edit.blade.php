@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        @if(isset($company))
        <h3>{{ trans('message.add_company') }}</h3>
        @else
        <h3>{{ trans('message.edit_company') }}</h3>
        @endif
    </div>
    <div class="card-body">
        <form class="row g-3" method="post" id="companyForm" enctype='multipart/form-data' action="{{ route('save_company') }}">
            @csrf
            @if(isset($company))
            <input type="hidden" name="id" value="{{ $company->id }}">
            @endif
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">{{ trans('message.company_name') }}</label>
                <input type="text" id="name" name="name" class="form-control @if($errors->has('name')) is-invalid @endif" value="@if(isset($company)){{ $company->name }}@endif">
                @if($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">{{ trans('message.email') }}</label>
                <input type="text" id="email" name="email" class="form-control @if($errors->has('email')) is-invalid @endif" value="@if(isset($company)){{ $company->email }}@endif">
                @if($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
                @endif
            </div>


            <div class="col-md-6">
                <label for="formFile" class="form-label">{{ trans('message.logo') }}</label>
                <input id="logo" type="file" name="logo" class="form-control @if($errors->has('logo')) is-invalid @endif" value="@if(isset($company)){{ $company->logo }}@endif">
                @if(isset($company) && !empty($company->logo))
                <div class="col-md-3 card" style="margin-top:15px">
                    <img src="{{asset('storage/logos/original').'/'.$company->logo}}" height="100" weidth="100">
                </div>
                @endif
                @if($errors->has('logo'))
                <div class="invalid-feedback">
                    {{ $errors->first('logo') }}
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">{{ trans('message.website') }}</label>
                <input id="website" type="text" name="website" class="form-control @if($errors->has('website')) is-invalid @endif" value="@if(isset($company)){{ $company->website }}@endif">
                @if($errors->has('website'))
                <div class="invalid-feedback">
                    {{ $errors->first('website') }}
                </div>
                @endif
            </div>

            <div class="col-12">
                <a href="{{ route('company') }}" class="btn btn-primary save-btn">{{ trans('message.cancel') }}</a>
                <button type="submit" class="btn btn-primary save-btn">{{ trans('message.save') }}</button>
            </div>
        </form>
    </div>
</div>


@endsection

@push('pageJs')
{!! JsValidator::formRequest('App\Http\Requests\CompanyRequest', '#companyForm') !!}
@endpush