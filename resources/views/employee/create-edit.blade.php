@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        @if(isset($employee))
        <h3>{{ trans('message.edit_employee') }}</h3>
        @else
        <h3>{{ trans('message.add_employee') }}</h3>
        @endif
    </div>
    <div class="card-body">
        <form class="row g-3" method="post" id="employeeForm" action="{{ route('save_employee') }}">
            @csrf
            @if(isset($employee))
            <input type="hidden" name="id" value="{{ $employee->id }}">
            @endif
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">{{ trans('message.first_name') }}</label>
                <input type="text" id="first_name" name="first_name" class="form-control @if($errors->has('first_name')) is-invalid @endif" value="@if(isset($employee)){{ $employee->first_name }}@endif">
                @if($errors->has('first_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('first_name') }}
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">{{ trans('message.last_name') }}</label>
                <input type="text" id="last_name" name="last_name" class="form-control @if($errors->has('last_name')) is-invalid @endif" value="@if(isset($employee)){{ $employee->last_name }}@endif">
                @if($errors->has('last_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('last_name') }}
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">{{ trans('message.company_name') }}</label>
                <select type="text" id="company_id" name="company_id" class="form-control @if($errors->has('company_id')) is-invalid @endif">
                    <option value="">Select Company</option>
                    @foreach($companies as $company)
                    <option value="{{ $company->id }}" @if(isset($employee) && $employee->company_id == $company->id) selected @endif>{{ $company->name }}</option>
                    @endforeach
                </select>
                @if($errors->has('company_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('company_id') }}
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <label for="inputPassword4" class="form-label">{{ trans('message.email') }}</label>
                <input type="text" id="email" name="email" class="form-control @if($errors->has('email')) is-invalid @endif" value="@if(isset($employee)){{ $employee->email }}@endif">
                @if($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
                @endif
            </div>


            <div class="col-md-6">
                <label for="phone" class="form-label">{{ trans('message.phone_number') }}</label>
                <input id="phone" type="text" name="phone" class="form-control @if($errors->has('phone')) is-invalid @endif" value="@if(isset($employee)){{ $employee->phone }}@endif">
                @if($errors->has('phone'))
                <div class="invalid-feedback">
                    {{ $errors->first('phone') }}
                </div>
                @endif
            </div>            

            <div class="col-12">
                <a href="{{ route('employees') }}" class="btn btn-primary save-btn">{{ trans('message.cancel') }}</a>
                <button type="submit" class="btn btn-primary save-btn">{{ trans('message.save') }}</button>
            </div>
        </form>
    </div>
</div>


@endsection

@push('pageJs')
{!! JsValidator::formRequest('App\Http\Requests\EmployeeRequest', '#employeeForm') !!}
@endpush