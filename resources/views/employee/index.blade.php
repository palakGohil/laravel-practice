@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3>{{ trans('message.employees') }}</h3>
        <a href="{{route('create_employee')}}" class="btn btn-primary">Create Employee</a>
    </div>
    @if(session()->get('success_message'))
    <div class="alert alert-success flash-msg" role="alert">
        <button type="button" class="close alert-success" data-dismiss="alert">×</button>
        {{ session()->get('success_message') }}
    </div>
    @endif
    @if(session()->get('fail_message'))
    <div class="alert alert-success flash-msg" role="alert">
        <button type="button" class="close alert-success" data-dismiss="alert">×</button>
        {{ session()->get('fail_message') }}
    </div>
    @endif
    <div class="card-body">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
        {{ $employees->links() }}
        </ul>
    </nav>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{ trans('message.first_name') }}</th>
                    <th scope="col">{{ trans('message.last_name') }}</th>
                    <th scope="col">{{ trans('message.company_name') }}</th>
                    <th scope="col">{{ trans('message.email') }}</th>
                    <th scope="col">{{ trans('message.phone_number') }}</th>
                    <th scope="col">{{ trans('message.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employees as $key => $employee)
                <tr>
                    <th>{{ $key + 1 }}</th>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->company_name }}</td>            
                    <td>{{ $employee->email }}</td>
                    <td>{{ $employee->phone }}</td>
                    <td><a href="{{ route('edit_employee',['id' => $employee->id]) }}" class="btn btn-success"> <img src="{{ asset('images/edit_black_24dp.svg') }}" alt="edit" height="15"> {{ trans('message.edit') }}</a>&nbsp;&nbsp;<a href="{{ route('delete_employee',['id' => $employee->id]) }}" onClick="javascript: return confirm('Are you sure you want to delete?');" class="btn btn-danger"> <img src="{{ asset('images/delete_black_24dp.svg') }}" alt="edit" height="15"> {{ trans('message.delete') }}</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection