<?php

namespace App\Helpers;


use App\Models\Admin\User;
use App\Repositories\User\UserRepository;
use Image;
use Illuminate\Support\Facades\Auth;


class Helpers {

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public static function uploadFile($request, $field, $path, $thumbPath = "",$width="", $height="",$defaultName="") {
     //   echo 'single';
    // dd($field);
        if (!empty($request->file($field))) {

            $icon = $request->file($field);
            
            if($defaultName){
                $field = pathinfo($icon->getClientOriginalName(), PATHINFO_FILENAME);
                $imagename = $field .'.' . $icon->getClientOriginalExtension();
            } else {
                $imagename = $field . '_' . time() . '.' . $icon->getClientOriginalExtension();
            }

            if ($thumbPath != "" && $icon->getClientOriginalExtension() != 'pdf') {

                if($width !='' && $height !=''){
                    $thumb_img = Image::make($icon->getRealPath())->resize($width, $height);
                }else{
                    $thumb_img = Image::make($icon->getRealPath())->resize(100, 100);
                }
                $thumb_img->save($thumbPath . '/' . $imagename, 100);
            }

            $icon->move($path, $imagename);

            return $imagename;
        } else {
            return FALSE;
        }
    }

}
