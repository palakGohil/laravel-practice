<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Carbon;
class CompanyController extends Controller
{
    protected $companyRepository;
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->middleware('auth');
        $this->companyRepository = $companyRepository;
    } 

    public function index(Request $request)
    {
        $params = array();
        $companies = $this->companyRepository->getByParams($params);
       // dd($companies);
        $currentPage = $companies->currentPage();
        return view('company.index',compact('companies','currentPage'));
    }

    public function create(Request $request)
    {
        return view('company.create-edit');
    }

    public function save(CompanyRequest $request)
    {
        $params = array();
        $params['id'] = request('id');
        $params['name'] = request('name');
        $params['email'] = request('email');
        if (isset($request->logo) && $request->logo != '') {
            $attachements = $request->logo;
            $logo = \App\Helpers\Helpers::uploadFile($request, 'logo', public_path('../storage/app/public/logos/original'));
            //   dd($logo);
            if ($logo != false) {
                $params['logo'] = $logo;
            }
        }
        $params['website'] = request('website');
        if (isset($request->id)) {
            $params['updated_at'] = Carbon::now();
        } else {
            $params['created_at'] = Carbon::now();
        }
        $city = $this->companyRepository->save($params);
        if ($city) {
            if (isset($params['id']))
                return redirect()->route('company')->with('success_message', 'Company edited successfully');
            else
                return redirect()->route('company')->with('success_message', 'Company added successfully');
        } else {
            return redirect()->route('company')->with('fail_message', 'Error in saving data. Please contact Administrator');
        }

    }

    public function edit(Request $request, $id)
    {
        $params = array();
        $params['where'] = ['id' => $id];
        $company = $this->companyRepository->getByParams($params)->first();
      //  dd($company);
        return view('company.create-edit',compact('company'));
    }

    public function delete(Request $request,$id)
    {
        $this->companyRepository->delete($id);
        return redirect()->back()->with('success_message', 'Company deleted successfully');
    }
}
