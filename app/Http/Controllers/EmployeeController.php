<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EmployeeRepository;
use App\Repositories\CompanyRepository;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Carbon;

class EmployeeController extends Controller
{
    protected $employeeRepository;
    protected $companyRepository;
    public function __construct(
        EmployeeRepository $employeeRepository,
        CompanyRepository $companyRepository
    ) {
        $this->middleware('auth');
        $this->employeeRepository = $employeeRepository;
        $this->companyRepository = $companyRepository;
    }

    public function index(Request $request)
    {
        $params = array();
        $params['select'] = ['employees.*','companies.id as company_id','companies.name as company_name'];
        $params['join'] = ['companies,companies.id,=,employees.company_id'];
        $employees = $this->employeeRepository->getByParams($params);

        return view('employee.index', compact('employees'));
    }

    public function create(Request $request)
    {
        $param = array();
        $companies = $this->companyRepository->getByParams($param);
        return view('employee.create-edit', compact('companies'));
    }

    public function save(EmployeeRequest $request)
    {
      //  dd('llaskl');
        $params = array();
        $params['id'] = request('id');
        $params['first_name'] = request('first_name');
        $params['last_name'] = request('last_name');
        $params['company_id'] = request('company_id');
        $params['email'] = request('email');
        $params['phone'] = request('phone');
        if (isset($request->id)) {
            $params['updated_at'] = Carbon::now();
        } else {
            $params['created_at'] = Carbon::now();
        }
        $employee = $this->employeeRepository->save($params);
        if ($employee) {
            if (isset($params['id']))
                return redirect()->route('employees')->with('success_message', 'Employee edited successfully');
            else
                return redirect()->route('employees')->with('success_message', 'Employee added successfully');
        } else {
            return redirect()->route('employees')->with('fail_message', 'Error in saving data. Please contact Administrator');
        }
    }

    public function edit(Request $request, $id)
    {
        $params = array();
        $params['where'] = ['id' => $id];
        $employee = $this->employeeRepository->getByParams($params)->first();
        //  dd($employee);
        $param = array();
        $companies = $this->companyRepository->getByParams($param);

        return view('employee.create-edit', compact('employee','companies'));
    }

    public function delete(Request $request, $id)
    {

        $this->employeeRepository->delete($id);
        return redirect()->back()->with('success_message', 'Employee deleted successfully');
    }
}
