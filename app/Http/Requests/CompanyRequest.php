<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'email',
            'logo' => 'image','mimes:jpg,png,jpeg','dimensions:min_width=100,min_height=100'
        ];

    }

    public function messages()
    {
        return [
            'required' => ':Attribute is required',
            'page_slug.required' => 'Title is required',
            'naem.required' => 'Description is required',
        ];
    }
}
