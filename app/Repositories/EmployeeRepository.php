<?php
namespace App\Repositories;

use App\Models\Employee;
use App\Repositories\BaseRepository;

class EmployeeRepository extends BaseRepository
{
    protected $employeeModel;

    public function __construct(Employee $employeeModel)
    {
        parent::__construct($employeeModel);
        $this->employeeModel = $employeeModel;
    }
}