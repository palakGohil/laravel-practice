<?php
namespace App\Repositories;

use App\Models\Company;
use App\Repositories\BaseRepository;

class CompanyRepository extends BaseRepository
{
    protected $companyModel;

    public function __construct(Company $companyModel)
    {
        parent::__construct($companyModel);
        $this->companyModel = $companyModel;
    }
}