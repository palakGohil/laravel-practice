<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['register' => false]);
Auth::routes();
//Route::get('dashboard', 'App\Http\Controllers\UserController@dashboard')->middleware('auth');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/companies',[CompanyController::class,'index'])->name('company');
Route::get('/create-company',[CompanyController::class,'create'])->name('create_company');
Route::post('save-company',[CompanyController::class,'save'])->name('save_company');
Route::get('/edit-company/{id}',[CompanyController::class,'edit'])->name('edit_company');
Route::get('delete-company/{id}',[CompanyController::class,'delete'])->name('delete_company');

//Employee routes
Route::get('employees',[EmployeeController::class,'index'])->name('employees');
Route::get('create-employee',[EmployeeController::class,'create'])->name('create_employee');
Route::post('save-employee',[EmployeeController::class,'save'])->name('save_employee');
Route::get('edit-employee/{id}',[EmployeeController::class,'edit'])->name('edit_employee');
Route::get('delete-employee/{id}',[EmployeeController::class,'delete'])->name('delete_employee');